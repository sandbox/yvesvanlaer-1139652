<?php
// $Id$

/**
 * @file
 * Admin functions for facebook_like_box.
 */

/**
 * Configure form block configuration
 */
function facebook_like_box_block_settings() {
  global $base_url;
  
    /*  * Facebook Page URL (?) || https://www.facebook.com/pages/Chicken-Rhythm/115147238506997 */
  $form['facebook_like_box_block_url'] = array(
    '#title' => t('Full Facebook page URL'),
    '#type' => 'textfield',
    '#default_value' => variable_get('facebook_like_box_block_url', $base_url),
    '#description' => t('URL of your facebook page. Example: http://www.facebook.com/pages/Drupal/8427738891')
  );
  $form['facebook_like_box_block'] = array(
    '#type' => 'fieldset',
    '#title' => 'Block configuration',
    '#collapsible' => false,
  );
  
  /*   * Width (?) || 292 px*/
  $form['facebook_like_box_block']['facebook_like_box_bl_width'] = array(
      '#title' => t('Width'),
      '#type' => 'textfield',
      '#default_value' => variable_get('facebook_like_box_bl_width', '292'),
      '#description' => t('Width of the widget in pixels'),
    );
  
/*  * Show Faces (?) || true / false*/
    $form['facebook_like_box_block']['facebook_like_box_bl_show_faces'] = array(
      '#type' => 'select',
      '#title' => t('Display faces in the box'),
      '#options' => array(TRUE => t('Show faces'), FALSE => t('Do not show faces')),
      '#default_value' => variable_get('facebook_like_box_bl_show_faces', TRUE),
      '#description' => t('Show profile pictures in the box.'),
    );
    
    /*  * Stream (?) || true / false*/
    $form['facebook_like_box_block']['facebook_like_box_bl_stream'] = array(
      '#type' => 'select',
      '#title' => t('Display stream in the box'),
      '#options' => array(TRUE => t('Show stream'), FALSE => t('Do not show stream')),
      '#default_value' => variable_get('facebook_like_box_bl_stream', TRUE),
      '#description' => t('Show stream in the box.'),
    );
    
	/** Header (?) || true / false*/
    $form['facebook_like_box_block']['facebook_like_box_bl_header'] = array(
      '#type' => 'select',
      '#title' => t('Display header in the box'),
      '#options' => array(TRUE => t('Show header'), FALSE => t('Do not show header')),
      '#default_value' => variable_get('facebook_like_box_bl_header', TRUE),
      '#description' => t('Show Find Us On Facebook above the box. Only works with stream or show faces enabled'),
    );
    
    
    /*  * Color Scheme (?) || light / dark */
    $form['facebook_like_box_block']['facebook_like_box_bl_colorscheme'] = array(
	  '#type' => 'select',
	  '#title' => t('Color scheme'),
	  '#options' => array('light' => t('Light'), 'dark' => t('Dark')),
	  '#default_value' => variable_get('facebook_like_box_bl_colorscheme', 'light'),
	  '#description' => t('The color scheme of box environment'),
    );
  return system_settings_form($form);
}
